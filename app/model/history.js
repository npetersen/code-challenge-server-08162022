const https = require('https');
const httpsAgent = new https.Agent({ rejectUnauthorized: false });
const axios = require('axios').default;

exports.getHistory = function(params, cb) {

    const objParams = params;
    const coinApiLocation = process.env.COIN_API_LOCATION + `/v1/ohlcv/${ objParams.symbolId }/history?period_id=${ objParams.periodId }&time_start=${ objParams.timeStart }&time_end=${ objParams.timeEnd }&limit=${ objParams.limit }`;
    const coinApiKey = process.env.COIN_API_KEY;

    let coinApiResponseError;
    let coinApiResponse;
    let headers = {
        'X-CoinAPI-Key': coinApiKey,
        'Accept': 'application/json',
        'Accept-Encoding': 'deflate, gzip'
    };

    console.log('Coin API Location: ', coinApiLocation);
    console.log('Coin API Key: ', coinApiKey);
    console.log('Headers: ', headers);

    axios.get(coinApiLocation, { httpsAgent: httpsAgent, headers: headers }).then(response => {

        if (response.status === 200) {

            coinApiResponseError = null;
            coinApiResponse = response.data;

        } else {

            throw new Error(response.data);

        }

        cb(coinApiResponseError, coinApiResponse);

    }).catch(error => {

        cb(error, null);

    })
}