const https = require('https');
const httpsAgent = new https.Agent({ rejectUnauthorized: false });
const axios = require('axios').default;

exports.getSymbols = function(cb) {

    const coinApiLocation = process.env.COIN_API_LOCATION + '/v1/symbols';
    const coinApiKey = process.env.COIN_API_KEY;

    let coinApiResponseError;
    let coinApiResponse;
    let headers = {
        'X-CoinAPI-Key': coinApiKey,
        'Accept': 'application/json',
        'Accept-Encoding': 'deflate, gzip'
    };

    axios.get(coinApiLocation, { httpsAgent: httpsAgent, headers: headers }).then(response => {

        if (response.status === 200) {

            coinApiResponseError = null;
            coinApiResponse = response.data;

        } else {

            throw new Error(response.data);

        }

        cb(coinApiResponseError, coinApiResponse);

    }).catch(error => {

        cb(error, null);

    })

}

exports.getFilteredSymbols = function(params, cb) {

    const objParams = params;
    const coinApiLocation = process.env.COIN_API_LOCATION + `/v1/symbols?filter_symbol_id=${ objParams.filterSymbolId }&filter_exchange_id=${ objParams.filterExchangeId }&filter_asset_id=${ objParams.filterAssetId }`;
    const coinApiKey = process.env.COIN_API_KEY;

    let coinApiResponseError;
    let coinApiResponse;
    let headers = {
        'X-CoinAPI-Key': coinApiKey,
        'Accept': 'application/json',
        'Accept-Encoding': 'deflate, gzip'
    };

    console.log('Coin API Location: ', coinApiLocation);
    console.log('Coin API Key: ', coinApiKey);
    console.log('Headers: ', headers);

    axios.get(coinApiLocation, { httpsAgent: httpsAgent, headers: headers }).then(response => {

        if (response.status === 200) {

            coinApiResponseError = null;
            coinApiResponse = response.data;

        } else {

            throw new Error(response.data);

        }

        cb(coinApiResponseError, coinApiResponse);

    }).catch(error => {

        cb(error, null);

    })
}

exports.getSymbolsByExchangeId = function(exchangeId, cb) {

    const coinApiLocation = process.env.COIN_API_LOCATION + `/v1/symbols/${ exchangeId }`;
    const coinApiKey = process.env.COIN_API_KEY;

    let coinApiResponseError;
    let coinApiResponse;
    let headers = {
        'X-CoinAPI-Key': coinApiKey,
        'Accept': 'application/json',
        'Accept-Encoding': 'deflate, gzip'
    };

    axios.get(coinApiLocation, { httpsAgent: httpsAgent, headers: headers }).then(response => {

        if (response.status === 200) {

            coinApiResponseError = null;
            coinApiResponse = response.data;

        } else {

            throw new Error(response.data);

        }

        cb(coinApiResponseError, coinApiResponse);

    }).catch(error => {

        cb(error, null);

    })

}