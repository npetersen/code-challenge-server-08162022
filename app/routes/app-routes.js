const history = require('../controller/history-controller');
const symbols = require('../controller/symbols-controller');

module.exports = app => {

    app.get('/history', history.getHistory);
    
    app.get('/history/:symbolId/:periodId/:timeStart/:timeEnd/:limit/:includeEmpty', history.getHistory); // http://localhost:8095/history/BITSTAMP_SPOT_BTC_USD/1DAY/2022-08-17T06:00:00.000Z/2022-08-18T05:59:59.999Z/100/false

    app.get('/symbols', symbols.getSymbols);
    
    app.get('/symbols/:filterSymbolId/:filterExchangeId/:filterAssetId', symbols.getFilteredSymbols);

    app.get('/symbols/:exchangeId', symbols.getSymbolsByExchangeId);

}