const model_history = require('../model/history');
const dayjs = require('dayjs');

exports.getHistory = (req, res) => {

    let params = {
        symbolId: req.params.symbolId || 'BITSTAMP_SPOT_BTC_USD',
        periodId: req.params.periodId || '1HRS',
        timeStart: req.params.timeStart || dayjs().startOf('day').toISOString(),
        timeEnd: req.params.timeEnd || dayjs().endOf('day').toISOString(),
        limit: req.params.limit || 100,
        includeEmpty: req.params.includeEmpty || false
    };

    // if (!req.params) {
    //     res.status(400).send();
    // } else {
    //     params = {
    //         symbolId: req.params.symbolId | 'BITSTAMP_SPOT_BTC_USD',
    //         periodId: req.params.periodId | '1MIN',
    //         timeStart: req.params.timeStart | dayjs().startOf('day').toISOString(),
    //         timeEnd: req.params.timeEnd | dayjs().toISOString,
    //         limit: req.params.limit | 100,
    //         includeEmpty: req.params.includeEmpty | false
    //     }
    // }

    model_history.getHistory(params, (error, objHistory) => {
        if (error) {
            console.log(error);
            res.status(500).send();
        } else {
            res.status(200).json(objHistory);
        }
    })

}