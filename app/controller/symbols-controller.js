const model_symbols = require('../model/symbols');

exports.getSymbols = (req, res) => {

    model_symbols.getSymbols((error, objSymbols) => {
        
        if (error) {

            console.error(error);
            res.status(500).send();

        } else {

            res.status(200).json(objSymbols);

        }

    });

}

exports.getFilteredSymbols = (req, res) => {

    let params = {
        filterSymbolId: req.params.filterSymbolId || 'KRAKENFTS_PERP_BTC_USD',
        filterExchangeId: req.params.filterExchangeId || 'KRAKENFTS',
        filterAssetId: req.params.filterAssetId || 'USD'
    };

    model_symbols.getFilteredSymbols(params, (error, objSymbols) => {

        if (error) {

            console.error(error);
            res.status(500).send();

        } else {

            res.status(200).json(objSymbols);

        }

    });

}

exports.getSymbolsByExchangeId = (req, res) => {

    let exchangeId = req.params.exchangeId;

    model_symbols.getSymbolsByExchangeId(exchangeId, (error, objSymbols) => {

        if (error) {

            console.error(error);
            res.status(500).send();

        } else {

            res.status(200).json(objSymbols);

        }

    })

}