const path = require('path');
const envFile = path.resolve(process.cwd(), `${process.env.NODE_ENV}.env`);
require('dotenv').config({ path: envFile });
const express = require('express');
const port = process.env.PORT;
const app = express();

// Parse requests of content-type - application/json
app.use(express.json());

const server = app.listen(port, () => {
    console.log(`Code Challenge RESTful API Sever running on port ${port}`);
});

app.get('/', (req, res) => {
    res.send('Code Challenge RESTful API Server');
});

require('./app/routes/app-routes')(app);